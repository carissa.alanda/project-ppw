from django.db import models

class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length=100)

class Peserta(models.Model):
    nama_peserta = models.CharField(max_length=100)
    kegiatan = models.ForeignKey(Kegiatan,on_delete=models.CASCADE, null=True, blank=True)
