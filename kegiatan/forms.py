from django import forms
from .models import Kegiatan, Peserta

class Form_Kegiatan(forms.Form):
    nama_kegiatan = forms.CharField(label = "Nama Kegiatan", 
                                max_length = 100, 
                                widget = forms.TextInput(attrs={
                                "placeholder": "Ex : Latihan Vokal", 
                                "class":"form-control",}))

class Form_Peserta(forms.Form):
    nama_peserta = forms.CharField(label = "Nama Peserta Kegiatan", 
                                max_length = 100, 
                                widget = forms.TextInput(attrs={
                                "placeholder": "Ex : Pewe", 
                                "class":"form-control",}))