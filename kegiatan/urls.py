from django.urls import path
from . import views

urlpatterns = [
    path('tambahKegiatan/',views.tambahKegiatan, name='tambahKegiatan'),
    path('listKegiatan/',views.listKegiatan,name = 'listKegiatan'),
    path('registrasi/<int:id>',views.registrasi, name='registrasi'),
]
