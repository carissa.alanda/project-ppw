from django.shortcuts import render, redirect
from .models import Kegiatan, Peserta
from . import forms
from . import views


def tambahKegiatan(request):
    form_tambah_kegiatan = forms.Form_Kegiatan()
    if request.method == "POST":
        input_form = forms.Form_Kegiatan(request.POST)
        if input_form.is_valid():
            data = input_form.cleaned_data #datanya masuk ke dict cleaned_data, karna pas di cek dia udah valid
            input_kegiatan = Kegiatan() #panggil class di models

            input_kegiatan.nama_kegiatan = data['nama_kegiatan'] #buat key value, data itu nama dict nya

            input_kegiatan.save()
            data_terbaru = Kegiatan.objects.all()

            return redirect('/listKegiatan')
    else:
        context = {'form':form_tambah_kegiatan} 
        return render(request, 'kegiatan/tambah_kegiatan.html', context)

def listKegiatan(request):
    kegiatan = Kegiatan.objects.all()
    peserta = Peserta.objects.all()
    context = {'list_kegiatan':kegiatan, 'list_peserta':peserta}
    return render(request, 'kegiatan/list_kegiatan.html', context)

def registrasi(request, id):
    form_tambah_peserta = forms.Form_Peserta()
    if request.method == "POST":
        input_form = forms.Form_Peserta(request.POST)
        if input_form.is_valid():
            data = input_form.cleaned_data
            input_peserta = Peserta()

            input_peserta.nama_peserta = data['nama_peserta']
            input_peserta.kegiatan = Kegiatan.objects.get(id=id)

            input_peserta.save()

            return redirect('/listKegiatan')
    
    else:
        context = {'form':form_tambah_peserta}
        return render(request, 'kegiatan/registrasi_peserta.html', context)


