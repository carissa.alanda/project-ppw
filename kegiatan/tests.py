from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Kegiatan, Peserta
from . import forms
from .views import tambahKegiatan, listKegiatan, registrasi



class TestModel(TestCase): #test untuk cek apakah data berhasil dimasukan ke database
    def test_apakah_ada_model_kegiatan(self):
      Kegiatan.objects.create(nama_kegiatan='nyanyi')
      hitung_jumlah_kegiatan = Kegiatan.objects.all().count()
      self.assertEquals(hitung_jumlah_kegiatan, 1)
    
    def test_apakah_ada_model_peserta(self):
      Peserta.objects.create(nama_peserta='Pewe')
      hitung_jumlah_peserta = Peserta.objects.all().count()
        
      self.assertEquals(hitung_jumlah_peserta, 1)

class TestForm(TestCase):
    def test_form_kegiatan_valid(self):
      form_tambah_kegiatan = forms.Form_Kegiatan(data={"nama_kegiatan":"nyanyi"})
      
      self.assertTrue(form_tambah_kegiatan.is_valid())
    
    def test_form_peserta_valid(self):
      form_tambah_peserta = forms.Form_Peserta(data={"nama_peserta":"pewe"})
        
      self.assertTrue(form_tambah_peserta.is_valid())
    
    def test_form_kegiatan_invalid(self):
      form_tambah_kegiatan = forms.Form_Kegiatan(data={})
        
      self.assertFalse(form_tambah_kegiatan.is_valid())
    
    def test_form_peserta_invalid(self):
      form_tambah_peserta = forms.Form_Peserta(data={})
        
      self.assertFalse(form_tambah_peserta.is_valid())

class TestURL(TestCase):
    def setUp(self):
      self.kegiatan = Kegiatan.objects.create(nama_kegiatan='nyanyi')
      self.tambahKegiatan = reverse("tambahKegiatan")
      self.listKegiatan = reverse("listKegiatan")
      self.registrasi = reverse("registrasi", args=[self.kegiatan.pk])

    def test_apakah_url_list_kegiatan_ada(self):
      found = resolve(self.listKegiatan)
      self.assertEqual(found.func, listKegiatan)
    
    def test_apakah_url_tambah_kegiatan_ada(self):
      found = resolve(self.tambahKegiatan)
      self.assertEqual(found.func, tambahKegiatan)
      
    def test_apakah_url_registrasi_ada(self):
      found = resolve(self.registrasi)
      self.assertEqual(found.func, registrasi)

class TestView(TestCase):

    def setUp(self):
      self.tambahKegiatan = reverse("tambahKegiatan")
      kegiatan = Kegiatan(nama_kegiatan="nyanyi")
      kegiatan.save()
      
    
    def test_apakah_di_halaman_list_kegiatan_ada_templatenya(self):
      response = Client().get('/listKegiatan/')
      self.assertTemplateUsed(response, 'kegiatan/list_kegiatan.html')
    
    def test_apakah_di_halaman_tambah_kegiatan_ada_templatenya(self):
      response = Client().get('/tambahKegiatan/')
      self.assertTemplateUsed(response, 'kegiatan/tambah_kegiatan.html')
    
    def test_apakah_di_halaman_registrasi_ada_templatenya(self):
      response = Client().get('/registrasi/1')
      self.assertTemplateUsed(response, 'kegiatan/registrasi_peserta.html')
    
    
    def test_post_tambah_kegiatan(self):
      response = Client().post(self.tambahKegiatan, {'nama_kegiatan':'nyanyi'}, follow=True)
      self.assertEqual(response.status_code, 200)
    
    def test_post_registrasi(self):
      response = Client().post('/registrasi/1', data={"nama_peserta":"pewe"})
      self.assertEqual(response.status_code, 302)