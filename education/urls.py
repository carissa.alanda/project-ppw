from django.urls import path

from . import views

app_name = 'education'

urlpatterns = [
    path('education/', views.my_edu, name='education'),
]
