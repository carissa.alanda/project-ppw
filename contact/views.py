from django.shortcuts import render


def contact_me(request):
    return render(request, 'contact/contact_me.html')

