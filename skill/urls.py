from django.urls import path

from . import views

app_name = 'skill'

urlpatterns = [
    path('skill/', views.my_skill, name='skill'),
]
