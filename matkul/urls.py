from django.urls import path
from . import views

urlpatterns = [
    path('tambahMatkul/',views.tambahMatkul, name='addMatkul'),
    path('listMatkul/',views.listMatkul,name = 'listMatkul'),
    path('hapusMatkul/<int:id>/',views.hapusMatkul, name='delete'),
    path('detailMatkul/<int:id>',views.detailMatkul, name='details'),
]
