from django.db import models

class Mata_Kuliah(models.Model):
    nama_matkul = models.CharField(max_length=100)
    nama_dosen = models.CharField(max_length=100)
    jumlah_sks = models.IntegerField()
    deskripsi_matkul = models.CharField(max_length=1000)
    semester_tahun = models.CharField(max_length=100)
    ruang_kelas = models.CharField(max_length=100)

