from django.shortcuts import render, redirect
from .models import Mata_Kuliah
from . import forms

def tambahMatkul(request):
    form = forms.Form_Matkul() #data dari form
    if request.method == "POST":
        input_form = forms.Form_Matkul(request.POST)
        if input_form.is_valid():
            data = input_form.cleaned_data #datanya masuk ke dict cleaned_data, karna pas di cek dia udah valid
            input_matkul = Mata_Kuliah() #panggil class di models

            input_matkul.nama_matkul = data['nama_matkul'] #buat key value, data itu nama dict nya
            input_matkul.nama_dosen = data['nama_dosen']
            input_matkul.jumlah_sks = data['jumlah_sks']
            input_matkul.deskripsi_matkul = data['deskripsi_matkul']
            input_matkul.semester_tahun = data['semester_tahun']
            input_matkul.ruang_kelas = data['ruang_kelas']

            input_matkul.save()
            data_terbaru = Mata_Kuliah.objects.all()

            return redirect('/listMatkul')
    else:
        matkul = Mata_Kuliah.objects.all()
        context = {'form':form,'matkul':matkul}
        return render(request, 'matkul/tambah_matkul.html', context) 

def listMatkul(request):
    all_data = Mata_Kuliah.objects.all()
    return render(request, 'matkul/list_matkul.html', {'ListMatkul':all_data})

def hapusMatkul(request, id):
    try:
        matkul_dihapus = Mata_Kuliah.objects.get(pk = id)
        matkul_dihapus.delete()
        return redirect ('/listMatkul')
    except:
        return redirect ('/listMatkul')

def detailMatkul(request,id):
    try:
        liatMatkul = Mata_Kuliah.objects.get(pk = id)
        print(liatMatkul)
        return render(request, 'matkul/detail_matkul.html', {'matkul':liatMatkul})
    except:
        return redirect('/listMatkul')


            


