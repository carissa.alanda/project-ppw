from django import forms
from .models import Mata_Kuliah

class Form_Matkul(forms.Form):
    nama_matkul = forms.CharField(label = "Nama Mata Kuliah", 
                                max_length = 100, 
                                widget = forms.TextInput(attrs={
                                "placeholder": "Ex : PPW", 
                                "class":"form-control",}))

    nama_dosen = forms.CharField(label = "Nama Dosen", 
                                max_length = 100, 
                                widget = forms.TextInput(attrs={
                                "placeholder": "Ex : Bapak Pewe", 
                                "class":"form-control",}))

    jumlah_sks = forms.IntegerField(label = "Jumlah SKS",  
                                widget = forms.NumberInput(attrs={
                                "placeholder": "Isi angka 1-6", 
                                "class":"form-control",
                                'min':'1',
                                'max':'6'}))

    deskripsi_matkul = forms.CharField(label = "Deskripsi Mata Kuliah", 
                                max_length = 1000, 
                                widget = forms.TextInput(attrs={ 
                                "class":"form-control",}))

    semester_tahun = forms.CharField(label = "Semester Tahun", 
                                max_length = 100, 
                                widget = forms.TextInput(attrs={
                                "placeholder": "Ex : Gasal 2019/2020", 
                                "class":"form-control",}))
    
    ruang_kelas = forms.CharField(label = "Ruang Kelas", 
                                max_length = 100, 
                                widget = forms.TextInput(attrs={
                                "placeholder": "Ex : 1101", 
                                "class":"form-control",}))